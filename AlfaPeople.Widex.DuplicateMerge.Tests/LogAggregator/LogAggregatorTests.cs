﻿using Logging = AlfaPeople.Widex.DuplicateMerge.Logging;
using AlfaPeople.Widex.DuplicateMerge.Tests.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlfaPeople.Widex.DuplicateMerge.Options;

namespace AlfaPeople.Widex.DuplicateMerge.Tests.LogAggregator
{
    [TestClass]
    public class LogAggregatorTests
    {
        [TestMethod]
        public void LogAggregator_CreateOnlyConsoleLog()
        {
            FakeLogger fakeLogger = new FakeLogger();
            
            string fileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TestExcelFiles", "ForMergeSmall.xlsx");

            CommandLineOptions options = new CommandLineOptions() { ExcelFilePath = fileName };

            Logging.LogAggregator logAggregator = new Logging.LogAggregator(options);

            Assert.AreEqual(logAggregator.Loggers.Count, 1);
            Assert.AreEqual(logAggregator.Loggers[0].GetType(), typeof(Logging.ConsoleLogger));
        }

        [TestMethod]
        public void LogAggregator_CreateConsoleAndExcelLog()
        {
            FakeLogger fakeLogger = new FakeLogger();
            string fileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TestExcelFiles", "ForMergeSmall.xlsx");

            CommandLineOptions options = new CommandLineOptions() { ExcelFilePath = fileName, LogFilePath = "test.txt" };

            Logging.LogAggregator logAggregator = new Logging.LogAggregator(options);

            Assert.AreEqual(logAggregator.Loggers.Count, 2);
            Assert.IsTrue(logAggregator.Loggers.Any(logger => logger.GetType() == typeof(Logging.ConsoleLogger)));
            Assert.IsTrue(logAggregator.Loggers.Any(logger => logger.GetType() == typeof(Logging.FileLogger)));            
        }

        [TestMethod]
        public void LogAggregator_LogginDoesNotThrowException()
        {
            FakeLogger fakeLogger = new FakeLogger();
            string fileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TestExcelFiles", "ForMergeSmall.xlsx");
            string logFileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TestExcelFiles", "log.txt");

            CommandLineOptions options = new CommandLineOptions() { ExcelFilePath = fileName, LogFilePath = "test.txt" };

            Logging.LogAggregator logAggregator = new Logging.LogAggregator(options);

            logAggregator.Log("test");

            File.Delete(logFileName);

            Assert.IsTrue(true);
        }
    }
}
