﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeOpenXml;
using System.IO;
using AlfaPeople.Widex.DuplicateMerge.Crm;
using Microsoft.Xrm.Sdk;
using AlfaPeople.Widex.DuplicateMerge.Models;
using AlfaPeople.Widex.DuplicateMerge.Options;

namespace AlfaPeople.Widex.DuplicateMerge.Tests.Integration
{
    [TestClass]
    public class Integration_ProcessDuplicateExcelFile
    {
        private ExcelWorksheet GetFirstWorksheetFromFile(string fileName)
        {
            string path = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TestExcelFiles", fileName);

            ExcelPackage excelPackage = new ExcelPackage(new FileInfo(path));
            return excelPackage.Workbook.Worksheets[1];
        }

        [TestMethod]
        
        public void Integration_Merge()
        {
            ExcelWorksheet workSheet = GetFirstWorksheetFromFile("ForMergeSmall.xlsx");
            DuplicateMerge.Excel.MergeRequestsFromExcel mergeRequests = new DuplicateMerge.Excel.MergeRequestsFromExcel();

            var mergeRequestRows = mergeRequests.GenerateMergeRequests(workSheet);

            CommandLineOptions options = new CommandLineOptions()
            {
                CrmUrl = "https://widextestusa.crm4.dynamics.com",
                UserName = "crmplugin@widex.com",
                Password = "Widex365"
            };

            IOrganizationService orgService = CrmConnectionFactory.GetConnection(options.CrmUrl, options.UserName, options.Password);

            AlfaPeople.Widex.DuplicateMerge.Crm.Merge merge = new AlfaPeople.Widex.DuplicateMerge.Crm.Merge(orgService);
            var cache = new AlfaPeople.Widex.DuplicateMerge.Crm.RecordCache(orgService, "contact");

            foreach (var mergeRequestRow in mergeRequestRows)
            {
                RecordWithState masterRecord = cache.FindInCache(mergeRequestRow.MasterId);

                foreach (var childId in mergeRequestRow.Children)
                {
                    RecordWithState childRecord = cache.FindInCache(childId);

                    merge.PerformMerge(masterRecord, childRecord, "lastname", "*Test*", mergeRequestRow.Fields);
                }
            }
        }
    }
}
