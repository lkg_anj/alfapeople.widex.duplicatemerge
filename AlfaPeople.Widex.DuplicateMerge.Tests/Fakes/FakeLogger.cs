﻿using AlfaPeople.Widex.DuplicateMerge.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Tests.Fakes
{
    public class FakeLogger : ILogger
    {
        public List<string> LoggedText { get; private set; }

        public FakeLogger()
        {
            LoggedText = new List<string>();
        }

        public void Log(string text)
        {
            LoggedText.Add(text);
        }
    }
}
