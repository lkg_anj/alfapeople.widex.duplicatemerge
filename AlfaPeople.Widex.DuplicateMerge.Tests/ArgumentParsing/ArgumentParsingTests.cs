﻿using AlfaPeople.Widex.DuplicateMerge.Logging;
using AlfaPeople.Widex.DuplicateMerge.Tests.Fakes;
using CommandLine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Tests.ArgumentParsing
{
    [TestClass]
    public class ArgumentParsingTests
    {
        //public class CommandLineOptions
        //{
        //    [Option('f', "file_name", Required = true, HelpText = "Excel file path. Example: \"C:\\temp\\duplicates.xslx\".")]
        //    public string ExcelFilePath { get; set; }

        //    [Option('u', "crm_user", Required = true, HelpText = "CRM user name. Example: \"admin@crm.com\".")]
        //    public string UserName { get; set; }

        //    [Option('p', "crm_password", Required = true, HelpText = "CRM user password. Example: \"MyPassword1\".")]
        //    public string Password { get; set; }

        //    [Option('a', "crm_address", Required = true, HelpText = "CRM address. Example: \"https://mycrm.dynamics.com\".")]
        //    public string CrmUrl { get; set; }

        //    [Option('l', "log_file_path", Required = false, HelpText = "Log file path. Example: \"logfile.txt\".")]
        //    public string LogFilePath { get; set; }

        //    [Option('p', "prefix_attribute", Required = false, HelpText = "Attribute to prefix after merge. Example: \"name\".")]
        //    public string PrefixAttribute { get; set; }

        //    [Option('t', "prefix_text", Required = false, HelpText = "Text to use as prefix. Example: \"*Merged*\".")]
        //    public string PrefixText { get; set; }
        //}

        //[TestMethod]
        //public void AgumentParsing_Empty()
        //{
        //    string[] args = new string[0];

        //    CommandLineOptions options = new CommandLineOptions();
        //    bool isValid = CommandLine.Parser.Default.ParseArgumentsStrict(args, options);

        //    Assert.IsFalse(isValid);
        //}

        //[TestMethod]
        //public void ArgumentParsing_MissingRequired()
        //{
        //    string[] args = "-f file.xlsx -u someuser@domain.com -a https://crm.dtam.com -l logfile.txt".Split(' ');

        //    CommandLineOptions options = new CommandLineOptions();
        //    bool isValid = CommandLine.Parser.Default.ParseArgumentsStrict(args, options);

        //    Assert.IsFalse(isValid);
        //}

        //[TestMethod]
        //public void ArgumentParsing_Correct()
        //{
        //    string[] args = "-f file.xlsx -u someuser@domain.com -p Password123 -a https://crm.dtam.com -l logfile.txt".Split(' ');

        //    CommandLineOptions options = new CommandLineOptions();
        //    bool isValid = CommandLine.Parser.Default.ParseArgumentsStrict(args, options);

        //    Assert.IsTrue(isValid);
        //}
    }
}
