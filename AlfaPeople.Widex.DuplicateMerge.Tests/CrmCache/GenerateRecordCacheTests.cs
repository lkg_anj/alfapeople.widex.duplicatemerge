﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using FakeXrmEasy;
using System.Collections.Generic;
using System.Linq;

namespace AlfaPeople.Widex.DuplicateMerge.Tests.CrmCache
{
    [TestClass]
    public class GenerateRecordCacheTests
    {

        private List<Entity> BuildContactList(int howMany)
        {
            List<Entity> contacts = new List<Entity>();
            for (int i = 0; i < howMany; i++)
            {
                int stateCode = i % 2 == 0 ? 1 : 0;

                Entity contact = new Entity("contact")
                {
                    Id = Guid.NewGuid()
                };
                contact["contactid"] = contact.Id;
                contact["statecode"] = new OptionSetValue(stateCode);
                contact["statuscode"] = new OptionSetValue(stateCode + 1);

                contacts.Add(contact);
            }

            return contacts;
        }

        [TestMethod]
        public void GenerateContactCache_CheckCount()
        {
            int nrOfContacts = 13000;

            List<Entity> contacts = BuildContactList(howMany: nrOfContacts);

            var context = new XrmFakedContext();
            context.Initialize(contacts);
            IOrganizationService orgService = context.GetOrganizationService();

            var cache = new AlfaPeople.Widex.DuplicateMerge.Crm.RecordCache(orgService, "contact");

            Assert.AreEqual(cache.CachedRecords.Length, nrOfContacts);
        }

        [TestMethod]
        public void GenerateContactCache_CheckCountWhen0()
        {
            int nrOfContacts = 0;

            List<Entity> contacts = BuildContactList(howMany: nrOfContacts);

            var context = new XrmFakedContext();
            context.Initialize(contacts);
            IOrganizationService orgService = context.GetOrganizationService();

            var cache = new AlfaPeople.Widex.DuplicateMerge.Crm.RecordCache(orgService, "contact");

            Assert.AreEqual(cache.CachedRecords.Length, nrOfContacts);
        }


        [TestMethod]
        public void GenerateContactCache_CheckState()
        {
            int nrOfContacts = 13000;

            List<Entity> contacts = BuildContactList(howMany: nrOfContacts);

            var context = new XrmFakedContext();
            context.Initialize(contacts);
            IOrganizationService orgService = context.GetOrganizationService();

            var cache = new AlfaPeople.Widex.DuplicateMerge.Crm.RecordCache(orgService, "contact");

            int fakedState10 = ((OptionSetValue)contacts.Where(c => c.Id == cache.CachedRecords[10].Id).First()["statecode"]).Value;
            int fakedState11 = ((OptionSetValue)contacts.Where(c => c.Id == cache.CachedRecords[11].Id).First()["statecode"]).Value;

            Assert.AreEqual(cache.CachedRecords[10].StateCode, fakedState10);
            Assert.AreEqual(cache.CachedRecords[11].StateCode, fakedState11);
        }

        [TestMethod]
        public void GenerateContactCache_CheckStatus()
        {
            int nrOfContacts = 13000;

            List<Entity> contacts = BuildContactList(howMany: nrOfContacts);

            var context = new XrmFakedContext();
            context.Initialize(contacts);
            IOrganizationService orgService = context.GetOrganizationService();

            var cache = new AlfaPeople.Widex.DuplicateMerge.Crm.RecordCache(orgService, "contact");

            int fakedStatus10 = ((OptionSetValue)contacts.Where(c => c.Id == cache.CachedRecords[10].Id).First()["statuscode"]).Value;
            int fakedStatus11 = ((OptionSetValue)contacts.Where(c => c.Id == cache.CachedRecords[11].Id).First()["statuscode"]).Value;

            Assert.AreEqual(cache.CachedRecords[10].StatusCode, fakedStatus10);
            Assert.AreEqual(cache.CachedRecords[11].StatusCode, fakedStatus11);
        }

        [TestMethod]
        public void GenerateContactCache_CheckType()
        {
            int nrOfContacts = 13000;

            List<Entity> contacts = BuildContactList(howMany: nrOfContacts);

            var context = new XrmFakedContext();
            context.Initialize(contacts);
            IOrganizationService orgService = context.GetOrganizationService();

            var cache = new AlfaPeople.Widex.DuplicateMerge.Crm.RecordCache(orgService, "contact");

            string type = cache.CachedRecords[3].Type;

            Assert.AreEqual("contact", type);
        }

        [TestMethod]
        public void GenerateContactCache_FindInCache()
        {
            int nrOfContacts = 13000;

            List<Entity> contacts = BuildContactList(howMany: nrOfContacts);

            var context = new XrmFakedContext();
            context.Initialize(contacts);
            IOrganizationService orgService = context.GetOrganizationService();

            var cache = new AlfaPeople.Widex.DuplicateMerge.Crm.RecordCache(orgService, "contact");

            var someRecord = cache.CachedRecords[11];

            var foundInCache = cache.FindInCache(someRecord.Id);

            Assert.AreEqual(someRecord.Id, foundInCache.Id);
        }
    }
}

