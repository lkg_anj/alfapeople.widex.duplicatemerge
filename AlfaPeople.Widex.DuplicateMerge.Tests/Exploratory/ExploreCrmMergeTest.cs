﻿using AlfaPeople.Widex.DuplicateMerge.Crm;
using AlfaPeople.Widex.DuplicateMerge.Options;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Tests.Exploratory
{
    [TestClass]
    public class Integration_ExploreCrmMergeTest
    {
        [TestMethod]
        [Ignore]
        public void Integration_Explore_Merge()
        {
            CommandLineOptions options = new CommandLineOptions()
            {
                CrmUrl = "https://bloomdev.crm4.dynamics.com",
                UserName = "crmplugin@widex.com",
                Password = "..."
            };

            IOrganizationService orgService = CrmConnectionFactory.GetConnection(options.CrmUrl, options.UserName, options.Password);

            MergeRequest mergeReq = new MergeRequest()
            {
                Target = new EntityReference("contact", new Guid("D31EFECB-D60E-E711-8101-5065F38BF2F1")),
                SubordinateId = new Guid("1AF12AEA-D60E-E711-8101-5065F38BF2F1"),
                UpdateContent = orgService.Retrieve("contact", new Guid("1AF12AEA-D60E-E711-8101-5065F38BF2F1"), 
                    new Microsoft.Xrm.Sdk.Query.ColumnSet("jobtitle", "ap_jobfunction")),
                PerformParentingChecks = false,

            };

            orgService.Execute(mergeReq);
        }
    }
}
