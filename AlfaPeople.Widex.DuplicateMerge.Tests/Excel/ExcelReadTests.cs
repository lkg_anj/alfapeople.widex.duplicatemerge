﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeOpenXml;
using System.IO;

namespace AlfaPeople.Widex.DuplicateMerge.Tests.Excel
{
    [TestClass]
    public class ExcelReadTests
    {
        private ExcelWorksheet GetFirstWorksheetFromFile(string fileName)
        {
            string path = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TestExcelFiles", fileName);

            ExcelPackage excelPackage = new ExcelPackage(new FileInfo(path));
            return excelPackage.Workbook.Worksheets[1];
        }

        private ExcelWorksheet GenerateSampleDataExcel()
        {
            var package = new ExcelPackage();
            var worksheet = package.Workbook.Worksheets.Add("Data");

            string entityNameBase = "entity";
            int[] childColumnsInRow = new[] { 2, 3, 1, 0 };

            for (int rowNr = 1; rowNr <= childColumnsInRow.Length; rowNr++)
            {
                worksheet.Cells[rowNr, 1].Value = $"{entityNameBase}_{rowNr}"; // Entity name
                worksheet.Cells[rowNr, 2].Value = $"fullname,adress1_street1";
                worksheet.Cells[rowNr, 3].Value = Guid.NewGuid().ToString("N"); // Master Id
                for (int i = 0; i < childColumnsInRow[rowNr - 1]; i++)
                {
                    worksheet.Cells[rowNr, 4 + i].Value = Guid.NewGuid().ToString("N");
                }
            }

            return worksheet;
        }

        private Guid GuidFromCell(ExcelRange cell)
        {
            return Guid.Parse((string)cell.Value);
        }

        [TestMethod]
        public void CellHasData_Yes()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            bool hasData = excel.CellHasGuidData(worksheet, 3, 3);

            Assert.IsTrue(hasData);
        }

        [TestMethod]
        public void CellHasData_NoEmpty()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            bool hasData = excel.CellHasGuidData(worksheet, 100, 100);

            Assert.IsFalse(hasData);
        }

        [TestMethod]
        public void CellHasData_NoNotAGuid()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            bool hasData = excel.CellHasGuidData(worksheet, 1, 1);

            Assert.IsFalse(hasData);
        }

        [TestMethod]
        public void EndOfFileReached_No()
        {
            ExcelWorksheet testWorksheet = GetFirstWorksheetFromFile("File with 5 rows.xlsx");

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            bool endOfData = excel.DataLimitReached(testWorksheet, 5);

            Assert.IsFalse(endOfData);
        }

        [TestMethod]
        public void EndOfFileReached_Yes()
        {
            ExcelWorksheet testWorksheet = GetFirstWorksheetFromFile("File with 5 rows.xlsx");

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            bool endOfData = excel.DataLimitReached(testWorksheet, 6);

            Assert.IsTrue(endOfData);
        }

        [TestMethod]
        public void EntityTypeParsing()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            var result = excel.BuildMergeRequestFromRow(worksheet, 2);

            Assert.AreEqual(worksheet.Cells[2, 1].Value.ToString(), result.EntityType);            
        }

        [TestMethod]
        public void MasterIdParsing()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            var result = excel.BuildMergeRequestFromRow(worksheet, 2);

            Assert.AreEqual(GuidFromCell(worksheet.Cells[2, 3]), result.MasterId);
        }

        [TestMethod]
        public void ChildrenParsing()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            var result = excel.BuildMergeRequestFromRow(worksheet, 2);

            // int[] childColumnsInRow = new[] { 2, 3, 1, 0 };
            Assert.AreEqual(3, result.Children.Length);

            int i = 0;
            foreach(var childId in result.Children)
            {
                Assert.AreEqual(GuidFromCell(worksheet.Cells[2, 4 + i]), result.Children[i]);
                i++;
            }
        }

        [TestMethod]
        public void ChildrenParsing_WhenNoChildren()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            var result = excel.BuildMergeRequestFromRow(worksheet, 4);

            Assert.IsTrue(result.Children.Length == 0);
        }

        [TestMethod]
        public void GenerateMergeRequests_FromWorksheet()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            var result = excel.GenerateMergeRequests(worksheet);

            Assert.IsTrue(result.Length > 0);
        }

        [TestMethod]
        public void GenerateMergeRequests_FromFile()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();

            string path = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TestExcelFiles", "ForMergeSmall.xlsx");

            var result = excel.GenerateMergeRequests(path);

            Assert.IsTrue(result.Length > 0);
        }

        [TestMethod]
        public void GenerateMergeRequests_FieldsAreParsed()
        {
            ExcelWorksheet worksheet = GenerateSampleDataExcel();

            DuplicateMerge.Excel.MergeRequestsFromExcel excel = new DuplicateMerge.Excel.MergeRequestsFromExcel();
            var result = excel.GenerateMergeRequests(worksheet);

            Assert.AreEqual(2, result[0].Fields.Length);
            Assert.AreEqual("fullname", result[0].Fields[0]);
            Assert.AreEqual("adress1_street1", result[0].Fields[1]);
        }
    }
}
