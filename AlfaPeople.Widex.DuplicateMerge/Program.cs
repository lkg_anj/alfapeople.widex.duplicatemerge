﻿using AlfaPeople.Widex.DuplicateMerge.Crm;
using AlfaPeople.Widex.DuplicateMerge.Logging;
using AlfaPeople.Widex.DuplicateMerge.Merge;
using AlfaPeople.Widex.DuplicateMerge.Models;
using AlfaPeople.Widex.DuplicateMerge.Options;
using CommandLine.Text;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandLineOptions options = new CommandLineOptions();
            bool isValid = CommandLine.Parser.Default.ParseArgumentsStrict(args, options);
            if (!isValid)
            {
                HelpText helpText = HelpText.AutoBuild(options);
                (new ConsoleLogger()).Log(helpText);
                return;
            }

            LogAggregator logAggregator = new LogAggregator(options);

            try
            {
                IOrganizationService orgService = CrmConnectionFactory.GetConnection(options.CrmUrl, options.UserName, options.Password);
                MergeFromExcelFile merge = new MergeFromExcelFile(logAggregator, orgService, options.ExcelFilePath);

                merge.Merge(options.PrefixAttribute, options.PrefixText);
            }
            catch(Exception ex)
            {
                logAggregator.Log($"General exception caught: {ex.Message}");
            }
        }
    }
}
