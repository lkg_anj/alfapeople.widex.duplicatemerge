﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Models
{
    public class RecordWithState
    {
        public string Type { get; set; }
        public Guid Id { get; set; }
        public int StateCode { get; set; }
        public int StatusCode { get; set; }
    }
}
