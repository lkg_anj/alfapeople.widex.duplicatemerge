﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Models
{
    public class MergeRequestRow
    {
        public string EntityType { get; set; }

        public string[] Fields { get; set; }

        public Guid MasterId { get; set; }

        public Guid[] Children { get; set; }
    }
}
