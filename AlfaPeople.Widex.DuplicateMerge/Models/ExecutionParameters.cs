﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Models
{
    public class ExecutionParameters
    {
        public string ExcelFilePath { get; set; }
        public string LogFilePath { get; set; }
    }
}
