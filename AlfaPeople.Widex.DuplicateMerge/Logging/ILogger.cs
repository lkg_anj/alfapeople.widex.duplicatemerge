﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Logging
{
    public interface ILogger
    {
        void Log(string text);
    }
}
