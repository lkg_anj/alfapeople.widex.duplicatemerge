﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Logging
{
    public class FileLogger : ILogger
    {
        string path = null;

        public FileLogger(string path)
        {
            this.path = path ?? throw new ArgumentNullException(nameof(path));
        }

        public void Log(string text)
        {
            string nowTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ff");

            string logLine = $"{nowTime}: {text}{Environment.NewLine}";

            File.AppendAllText(path, logLine);
        }
    }
}
