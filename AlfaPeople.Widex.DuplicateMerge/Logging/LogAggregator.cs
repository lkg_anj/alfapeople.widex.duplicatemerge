﻿using AlfaPeople.Widex.DuplicateMerge.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Logging
{
    public class LogAggregator : ILogger
    {
        public List<ILogger> Loggers { get; private set; }

        public LogAggregator(CommandLineOptions options)
        {
            Loggers = new List<ILogger>();

            Loggers.Add(new ConsoleLogger());
            if(options.LogFilePath != null)
            {
                Loggers.Add(new FileLogger(options.LogFilePath));
            }
        }

        public void Log(string text)
        {
            foreach(ILogger logger in Loggers)
            {
                logger.Log(text);
            }
        }
    }
}
