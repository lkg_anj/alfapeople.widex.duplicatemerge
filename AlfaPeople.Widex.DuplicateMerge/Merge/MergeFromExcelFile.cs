﻿using AlfaPeople.Widex.DuplicateMerge.Crm;
using AlfaPeople.Widex.DuplicateMerge.Logging;
using AlfaPeople.Widex.DuplicateMerge.Models;
using Microsoft.Xrm.Sdk;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Merge
{
    public class MergeFromExcelFile
    {
        private ILogger logger = null;
        private IOrganizationService orgService = null;
        private string excelFilePath = null;

        public MergeFromExcelFile(ILogger logger, IOrganizationService orgService, string excelFilePath)
        {
            this.logger = logger != null ? logger : throw new ArgumentNullException(nameof(logger));
            this.orgService = orgService != null ? orgService : throw new ArgumentNullException(nameof(orgService));
            this.excelFilePath = !String.IsNullOrWhiteSpace(excelFilePath) ? excelFilePath : throw new ArgumentNullException(nameof(excelFilePath));
        }

        public void Merge(string prefixAttribute, string prefixValue)
        {
            ExcelWorksheet workSheet = GetFirstWorksheetFromFile(excelFilePath);
            DuplicateMerge.Excel.MergeRequestsFromExcel mergeRequests = new DuplicateMerge.Excel.MergeRequestsFromExcel();

            var mergeRequestRows = mergeRequests.GenerateMergeRequests(workSheet);
            
            Crm.Merge merge = new Crm.Merge(orgService);

            string type = workSheet.Cells[2, 1].Value.ToString(); // TODO make more generic

            RecordCache cache = new RecordCache(orgService, type);


            foreach (var mergeRequestRow in mergeRequestRows)
            {
                try
                {
                    logger.Log($"Processing master {mergeRequestRow.MasterId} with {mergeRequestRow.Children.Length} children to merge");
                    RecordWithState masterRecord = cache.FindInCache(mergeRequestRow.MasterId);

                    int childNr = 1;
                    foreach (var childId in mergeRequestRow.Children)
                    {
                        try
                        {
                            logger.Log($"Merging {childId} into {mergeRequestRow.MasterId} #{childNr}/{mergeRequestRow.Children.Length}");
                            childNr++;

                            RecordWithState childRecord = cache.FindInCache(childId);

                            merge.PerformMerge(masterRecord, childRecord, prefixAttribute, prefixValue, mergeRequestRow.Fields);
                        }
                        catch (Exception ex)
                        {
                            logger.Log($"Error while processing mergeRequest childRecord: {ex.Message}");
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Log($"Error while processing mergeRequest: {ex.Message}");
                }
            }
        }

        private ExcelWorksheet GetFirstWorksheetFromFile(string fileName)
        {
            ExcelPackage excelPackage = new ExcelPackage(new FileInfo(fileName));

            return excelPackage.Workbook.Worksheets[1];
        }
    }
}

   

