﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Options
{
    public class CommandLineOptions
    {
        [Option('f', "file_name", Required = true, HelpText = "Excel file path. Example: \"C:\\temp\\duplicates.xslx\".")]
        public string ExcelFilePath { get; set; }

        [Option('u', "crm_user", Required = true, HelpText = "CRM user name. Example: \"admin@crm.com\".")]
        public string UserName { get; set; }

        [Option('p', "crm_password", Required = true, HelpText = "CRM user password. Example: \"MyPassword1\".")]
        public string Password { get; set; }

        [Option('a', "crm_url", Required = true, HelpText = "CRM address. Example: \"https://mycrm.dynamics.com\".")]
        public string CrmUrl { get; set; }

        [Option('l', "log_file_path", Required = false, HelpText = "Log file path. Example: \"logfile.txt\".")]
        public string LogFilePath { get; set; }

        [Option('r', "prefix_attribute", Required = false, HelpText = "Attribute to prefix after merge. Example: \"name\".")]
        public string PrefixAttribute { get; set; }

        [Option('t', "prefix_text", Required = false, HelpText = "Text to use as prefix. Example: \"*Merged*\".")]
        public string PrefixText { get; set; }
    }
}
