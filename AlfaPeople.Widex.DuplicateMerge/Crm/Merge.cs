﻿using AlfaPeople.Widex.DuplicateMerge.Models;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Query;

namespace AlfaPeople.Widex.DuplicateMerge.Crm
{
    public class Merge
    {
        private readonly IOrganizationService orgService;

        public Merge(IOrganizationService orgService)
        {
            this.orgService = orgService;
        }        

        public void PerformMerge(RecordWithState master, RecordWithState mergeWith, string prefixAttribute, string prefixValue, string[] fields)
        {
            if (IsDeactivated(master)) { Activate(master); }
            if (IsDeactivated(mergeWith)) { Activate(mergeWith); }

            try
            {
                Entity updateContent = GetUpdateContentSoItDoesntOverrideParent(master, mergeWith, fields);

                MergeRequest mergeRequest = new MergeRequest()
                {
                    Target = new EntityReference(mergeWith.Type, master.Id),
                    SubordinateId = mergeWith.Id,
                    UpdateContent = updateContent,
                    PerformParentingChecks = false
                };

                orgService.Execute(mergeRequest);

                AppendPrefixIfRequired(prefixAttribute, prefixValue, mergeWith);
            }
            finally
            {
                if (IsDeactivated(master)) { Deactivate(master); }                
            }
        }

        private Entity GetUpdateContentSoItDoesntOverrideParent(RecordWithState master, RecordWithState mergeWith, string[] fields)
        {
            Entity updateContent = new Entity(mergeWith.Type);

            if (fields != null && fields.Length > 0)
            {
                Entity masterContent = orgService.Retrieve(master.Type, master.Id, new ColumnSet(fields));

                List<string> mergedFields = new List<string>();
                foreach (string field in fields)
                {
                    if (!masterContent.Attributes.Contains(field)
                        || masterContent.Attributes[field] == null
                        || masterContent.Attributes[field].ToString() == "")
                    {
                        mergedFields.Add(field);
                    }
                }

                if (mergedFields.Count > 0)
                {
                    updateContent = orgService.Retrieve(mergeWith.Type, mergeWith.Id, new ColumnSet(mergedFields.ToArray()));
                }
            }

            return updateContent;
        }

        private void AppendPrefixIfRequired(string prefixAttribute, string prefixValue, RecordWithState mergeWith)
        {
            if(String.IsNullOrWhiteSpace(prefixAttribute) || String.IsNullOrWhiteSpace(prefixValue))
            {
                return;
            }

            prefixAttribute = prefixAttribute.ToLower();

            Entity entity = orgService.Retrieve(mergeWith.Type, mergeWith.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(prefixAttribute));
            string currentValue = entity.Attributes.Contains(prefixAttribute) ? (string)entity[prefixAttribute] : "";

            string newValue = (prefixValue ?? "") + currentValue;

            Entity entityToUpdate = new Entity(mergeWith.Type);
            entityToUpdate.Id = mergeWith.Id;
            entityToUpdate[prefixAttribute] = newValue;

            orgService.Update(entityToUpdate);
        }

        private bool IsDeactivated(RecordWithState mergeWith)
        {
            // For all supported entities (account, contact, lead and incident)
            // the active state == 0

            return mergeWith.StateCode != 0;
        }

        private void Activate(RecordWithState mergeWith)
        {
            // For all supported entities (account, contact, lead and incident)
            // the active can be state == 0, status = 1
            SetStateRequest req = new SetStateRequest()
            {
                EntityMoniker = new EntityReference(mergeWith.Type, mergeWith.Id),
                State = new OptionSetValue(0),
                Status = new OptionSetValue(1)
            };
            orgService.Execute(req);
        }

        private void Deactivate(RecordWithState mergeWith)
        {
            SetStateRequest req = new SetStateRequest()
            {
                EntityMoniker = new EntityReference(mergeWith.Type, mergeWith.Id),
                State = new OptionSetValue(mergeWith.StateCode),
                Status = new OptionSetValue(mergeWith.StatusCode)
            };
            orgService.Execute(req);
        }
    }
}
