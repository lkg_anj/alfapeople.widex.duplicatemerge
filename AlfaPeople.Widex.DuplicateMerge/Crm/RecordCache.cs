﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using AlfaPeople.Widex.DuplicateMerge.Models;
using Microsoft.Xrm.Sdk.Query;

namespace AlfaPeople.Widex.DuplicateMerge.Crm
{
    public class RecordCache
    {
        private IOrganizationService orgService;
        private string entityName;
        private RecordWithState[] cachedRecords = null;

        public RecordCache(IOrganizationService orgService, string entityName)
        {
            this.orgService = orgService;
            this.entityName = entityName;
        }

        public RecordWithState[] CachedRecords
        {
            get
            {
                if (cachedRecords == null)
                {
                    List<RecordWithState> retList = new List<RecordWithState>();

                    QueryExpression query = new QueryExpression(entityName) { NoLock = true };
                    query.ColumnSet = new ColumnSet("statecode", "statuscode");

                    List <Entity> allEntities = GetAllWithPaging(query, orgService);

                    allEntities.ForEach(e =>
                    {
                        retList.Add(new RecordWithState()
                        {
                            Id = e.Id,
                            StateCode = ((OptionSetValue)e["statecode"]).Value,
                            StatusCode = ((OptionSetValue)e["statuscode"]).Value,
                            Type = entityName
                        });
                    });

                    cachedRecords = retList.ToArray();
                }

                return cachedRecords;
            }            
        } 
        
        public RecordWithState FindInCache(Guid id)
        {
            return CachedRecords.FirstOrDefault(r => r.Id == id);
        }

        private static List<Entity> GetAllWithPaging(QueryExpression query, IOrganizationService orgService)
        {
            List<Entity> entities = new List<Entity>();

            int pageNr = 1;

            query.PageInfo = new PagingInfo();
            query.PageInfo.PageNumber = pageNr;
            query.PageInfo.Count = 5000;

            while (true)
            {
                EntityCollection ecoll = orgService.RetrieveMultiple(query);

                entities.AddRange(ecoll.Entities);

                if (ecoll.MoreRecords)
                {
                    pageNr++;
                    query.PageInfo.PageNumber = pageNr;
                    query.PageInfo.PagingCookie = ecoll.PagingCookie;
                }
                else
                {
                    break;
                }
            }

            return entities;
        }
    }
}
