﻿using AlfaPeople.Widex.DuplicateMerge.Options;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Crm
{
    public class CrmConnectionFactory
    {
        private static IOrganizationService orgService = null;

        public static IOrganizationService GetConnection(string crmUrl, string userName, string password)
        {
            if (orgService == null)
            {
                string connectionString = $@"Url={crmUrl};User ID={userName}; Password={password}; authtype=Office365";
                CrmServiceClient crmServiceClient = new CrmServiceClient(connectionString);

                if (crmServiceClient == null)
                {
                    throw new Exception("Count not generate CrmServiceClient");
                }

                if (crmServiceClient.OrganizationServiceProxy == null)
                {
                    throw new Exception("crmServiceClient.OrganizationServiceProxy == null");
                }

                orgService = crmServiceClient.OrganizationServiceProxy;
            }

            return orgService;
        }
    }
}
