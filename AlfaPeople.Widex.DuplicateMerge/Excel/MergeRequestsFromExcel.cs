﻿using AlfaPeople.Widex.DuplicateMerge.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaPeople.Widex.DuplicateMerge.Excel
{
    public class MergeRequestsFromExcel
    {
        private readonly int dataStartsInRow = 2;
        private readonly int entityTypeIsInColumn = 1;
        private readonly int fieldsAreInColumnColumn = 2;
        private readonly int masterIdIsInColumn = 3;
        private readonly int childIdsStartInColumn = 4;       

        public MergeRequestRow[] GenerateMergeRequests(string fileName)
        {
            ExcelPackage excelPackage = new ExcelPackage(new FileInfo(fileName));
            ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets[1];

            return GenerateMergeRequests(excelWorksheet);
        }

        public MergeRequestRow[] GenerateMergeRequests(ExcelWorksheet excelWorksheet)
        {
            List<MergeRequestRow> retList = new List<MergeRequestRow>();

            for (int rowNr = dataStartsInRow; true; rowNr++)
            {
                if (DataLimitReached(excelWorksheet, rowNr))
                {
                    break;
                }

                MergeRequestRow mergeRequest = BuildMergeRequestFromRow(excelWorksheet, rowNr);
                retList.Add(mergeRequest);
            }

            return retList.ToArray();
        }

        public MergeRequestRow BuildMergeRequestFromRow(ExcelWorksheet excelWorksheet, int rowNr)
        {
            MergeRequestRow mergeRequest = new MergeRequestRow()
            {
                EntityType = (string)excelWorksheet.Cells[rowNr, entityTypeIsInColumn].Value,
                MasterId = GuidFromCell(excelWorksheet.Cells[rowNr, masterIdIsInColumn])
            };
            
            mergeRequest.Children = GetChildren(excelWorksheet, rowNr);
            mergeRequest.Fields = GetFields(excelWorksheet, rowNr);

            return mergeRequest;
        }

        private Guid[] GetChildren(ExcelWorksheet excelWorksheet, int rowNr)
        {
            List<Guid> childIds = new List<Guid>();
            for (int colNr = childIdsStartInColumn; ; colNr++)
            {
                if (!CellHasGuidData(excelWorksheet, rowNr, colNr))
                {
                    break;
                }
                childIds.Add(GuidFromCell(excelWorksheet.Cells[rowNr, colNr]));
            }

            return childIds.ToArray();
        }

        private string[] GetFields(ExcelWorksheet excelWorksheet, int rowNr)
        {
            string data = excelWorksheet.Cells[rowNr, fieldsAreInColumnColumn].Value.ToString();
            if (String.IsNullOrWhiteSpace(data))
            {
                return null;
            }
            else
            {
                return data.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public bool CellHasGuidData(ExcelWorksheet excelWorksheet, int rowNr, int colNr)
        {
            object value = excelWorksheet.Cells[rowNr, colNr].Value;

            return value != null
                   && Guid.TryParse(value.ToString(), out Guid g);
        }

        public bool DataLimitReached(ExcelWorksheet excelWorksheet, int rowNr)
        {
            return excelWorksheet.Cells[rowNr, 1].Value == null
                   || String.IsNullOrEmpty(excelWorksheet.Cells[rowNr, 1].Value.ToString());
        }

        private Guid GuidFromCell(ExcelRange cell)
        {
            return Guid.Parse(cell.Value.ToString());
        }
    }
}
