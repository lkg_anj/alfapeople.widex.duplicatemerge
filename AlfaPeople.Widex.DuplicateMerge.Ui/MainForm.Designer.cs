﻿namespace AlfaPeople.Widex.DuplicateMerge.Ui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbFileName = new System.Windows.Forms.Label();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.bPickFileName = new System.Windows.Forms.Button();
            this.tbCrmUser = new System.Windows.Forms.TextBox();
            this.lbCrmUser = new System.Windows.Forms.Label();
            this.tbCrmPassword = new System.Windows.Forms.TextBox();
            this.lbCrmPassword = new System.Windows.Forms.Label();
            this.tbCrmUrl = new System.Windows.Forms.TextBox();
            this.lbCrmUrl = new System.Windows.Forms.Label();
            this.tbLogFile = new System.Windows.Forms.TextBox();
            this.lbLogFile = new System.Windows.Forms.Label();
            this.tbPrefixAttr = new System.Windows.Forms.TextBox();
            this.lbPrefixAttr = new System.Windows.Forms.Label();
            this.tbPrefixText = new System.Windows.Forms.TextBox();
            this.lbPrefixText = new System.Windows.Forms.Label();
            this.bPickLogFile = new System.Windows.Forms.Button();
            this.lbCommand = new System.Windows.Forms.Label();
            this.tbCommand = new System.Windows.Forms.TextBox();
            this.ofdExcel = new System.Windows.Forms.OpenFileDialog();
            this.ofdLog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // lbFileName
            // 
            this.lbFileName.AutoSize = true;
            this.lbFileName.Location = new System.Drawing.Point(12, 29);
            this.lbFileName.Name = "lbFileName";
            this.lbFileName.Size = new System.Drawing.Size(61, 13);
            this.lbFileName.TabIndex = 0;
            this.lbFileName.Text = "File Name*:";
            // 
            // tbFileName
            // 
            this.tbFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFileName.Location = new System.Drawing.Point(75, 26);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(379, 20);
            this.tbFileName.TabIndex = 1;
            this.tbFileName.TextChanged += new System.EventHandler(this.tbInput_TextChanged);
            // 
            // bPickFileName
            // 
            this.bPickFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bPickFileName.Location = new System.Drawing.Point(464, 24);
            this.bPickFileName.Name = "bPickFileName";
            this.bPickFileName.Size = new System.Drawing.Size(27, 23);
            this.bPickFileName.TabIndex = 2;
            this.bPickFileName.Text = "...";
            this.bPickFileName.UseVisualStyleBackColor = true;
            this.bPickFileName.Click += new System.EventHandler(this.bPickFileName_Click);
            // 
            // tbCrmUser
            // 
            this.tbCrmUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCrmUser.Location = new System.Drawing.Point(75, 52);
            this.tbCrmUser.Name = "tbCrmUser";
            this.tbCrmUser.Size = new System.Drawing.Size(379, 20);
            this.tbCrmUser.TabIndex = 4;
            this.tbCrmUser.TextChanged += new System.EventHandler(this.tbInput_TextChanged);
            // 
            // lbCrmUser
            // 
            this.lbCrmUser.AutoSize = true;
            this.lbCrmUser.Location = new System.Drawing.Point(12, 55);
            this.lbCrmUser.Name = "lbCrmUser";
            this.lbCrmUser.Size = new System.Drawing.Size(63, 13);
            this.lbCrmUser.TabIndex = 3;
            this.lbCrmUser.Text = "CRM User*:";
            // 
            // tbCrmPassword
            // 
            this.tbCrmPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCrmPassword.Location = new System.Drawing.Point(75, 78);
            this.tbCrmPassword.Name = "tbCrmPassword";
            this.tbCrmPassword.Size = new System.Drawing.Size(379, 20);
            this.tbCrmPassword.TabIndex = 6;
            this.tbCrmPassword.TextChanged += new System.EventHandler(this.tbInput_TextChanged);
            // 
            // lbCrmPassword
            // 
            this.lbCrmPassword.AutoSize = true;
            this.lbCrmPassword.Location = new System.Drawing.Point(12, 81);
            this.lbCrmPassword.Name = "lbCrmPassword";
            this.lbCrmPassword.Size = new System.Drawing.Size(62, 13);
            this.lbCrmPassword.TabIndex = 5;
            this.lbCrmPassword.Text = "CRM Pwd*:";
            // 
            // tbCrmUrl
            // 
            this.tbCrmUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCrmUrl.Location = new System.Drawing.Point(75, 104);
            this.tbCrmUrl.Name = "tbCrmUrl";
            this.tbCrmUrl.Size = new System.Drawing.Size(379, 20);
            this.tbCrmUrl.TabIndex = 8;
            this.tbCrmUrl.TextChanged += new System.EventHandler(this.tbInput_TextChanged);
            // 
            // lbCrmUrl
            // 
            this.lbCrmUrl.AutoSize = true;
            this.lbCrmUrl.Location = new System.Drawing.Point(12, 107);
            this.lbCrmUrl.Name = "lbCrmUrl";
            this.lbCrmUrl.Size = new System.Drawing.Size(63, 13);
            this.lbCrmUrl.TabIndex = 7;
            this.lbCrmUrl.Text = "CRM URL*:";
            // 
            // tbLogFile
            // 
            this.tbLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLogFile.Location = new System.Drawing.Point(75, 130);
            this.tbLogFile.Name = "tbLogFile";
            this.tbLogFile.Size = new System.Drawing.Size(379, 20);
            this.tbLogFile.TabIndex = 10;
            this.tbLogFile.TextChanged += new System.EventHandler(this.tbInput_TextChanged);
            // 
            // lbLogFile
            // 
            this.lbLogFile.AutoSize = true;
            this.lbLogFile.Location = new System.Drawing.Point(12, 133);
            this.lbLogFile.Name = "lbLogFile";
            this.lbLogFile.Size = new System.Drawing.Size(47, 13);
            this.lbLogFile.TabIndex = 9;
            this.lbLogFile.Text = "Log File:";
            // 
            // tbPrefixAttr
            // 
            this.tbPrefixAttr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPrefixAttr.Location = new System.Drawing.Point(75, 156);
            this.tbPrefixAttr.Name = "tbPrefixAttr";
            this.tbPrefixAttr.Size = new System.Drawing.Size(379, 20);
            this.tbPrefixAttr.TabIndex = 12;
            this.tbPrefixAttr.TextChanged += new System.EventHandler(this.tbInput_TextChanged);
            // 
            // lbPrefixAttr
            // 
            this.lbPrefixAttr.AutoSize = true;
            this.lbPrefixAttr.Location = new System.Drawing.Point(12, 159);
            this.lbPrefixAttr.Name = "lbPrefixAttr";
            this.lbPrefixAttr.Size = new System.Drawing.Size(55, 13);
            this.lbPrefixAttr.TabIndex = 11;
            this.lbPrefixAttr.Text = "Prefix Attr:";
            // 
            // tbPrefixText
            // 
            this.tbPrefixText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPrefixText.Location = new System.Drawing.Point(75, 182);
            this.tbPrefixText.Name = "tbPrefixText";
            this.tbPrefixText.Size = new System.Drawing.Size(379, 20);
            this.tbPrefixText.TabIndex = 14;
            this.tbPrefixText.TextChanged += new System.EventHandler(this.tbInput_TextChanged);
            // 
            // lbPrefixText
            // 
            this.lbPrefixText.AutoSize = true;
            this.lbPrefixText.Location = new System.Drawing.Point(12, 185);
            this.lbPrefixText.Name = "lbPrefixText";
            this.lbPrefixText.Size = new System.Drawing.Size(60, 13);
            this.lbPrefixText.TabIndex = 13;
            this.lbPrefixText.Text = "Prefix Text:";
            // 
            // bPickLogFile
            // 
            this.bPickLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bPickLogFile.Location = new System.Drawing.Point(464, 128);
            this.bPickLogFile.Name = "bPickLogFile";
            this.bPickLogFile.Size = new System.Drawing.Size(27, 23);
            this.bPickLogFile.TabIndex = 15;
            this.bPickLogFile.Text = "...";
            this.bPickLogFile.UseVisualStyleBackColor = true;
            this.bPickLogFile.Click += new System.EventHandler(this.bPickLogFile_Click);
            // 
            // lbCommand
            // 
            this.lbCommand.AutoSize = true;
            this.lbCommand.Location = new System.Drawing.Point(12, 242);
            this.lbCommand.Name = "lbCommand";
            this.lbCommand.Size = new System.Drawing.Size(57, 13);
            this.lbCommand.TabIndex = 16;
            this.lbCommand.Text = "Command:";
            // 
            // tbCommand
            // 
            this.tbCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCommand.Location = new System.Drawing.Point(12, 268);
            this.tbCommand.Multiline = true;
            this.tbCommand.Name = "tbCommand";
            this.tbCommand.Size = new System.Drawing.Size(479, 58);
            this.tbCommand.TabIndex = 17;
            // 
            // ofdExcel
            // 
            this.ofdExcel.Filter = "Excel Files|*.xlsx";
            // 
            // ofdLog
            // 
            this.ofdLog.CheckFileExists = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 338);
            this.Controls.Add(this.tbCommand);
            this.Controls.Add(this.lbCommand);
            this.Controls.Add(this.bPickLogFile);
            this.Controls.Add(this.tbPrefixText);
            this.Controls.Add(this.lbPrefixText);
            this.Controls.Add(this.tbPrefixAttr);
            this.Controls.Add(this.lbPrefixAttr);
            this.Controls.Add(this.tbLogFile);
            this.Controls.Add(this.lbLogFile);
            this.Controls.Add(this.tbCrmUrl);
            this.Controls.Add(this.lbCrmUrl);
            this.Controls.Add(this.tbCrmPassword);
            this.Controls.Add(this.lbCrmPassword);
            this.Controls.Add(this.tbCrmUser);
            this.Controls.Add(this.lbCrmUser);
            this.Controls.Add(this.bPickFileName);
            this.Controls.Add(this.tbFileName);
            this.Controls.Add(this.lbFileName);
            this.Name = "MainForm";
            this.Text = "Widex Duplicate Merge UI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbFileName;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Button bPickFileName;
        private System.Windows.Forms.TextBox tbCrmUser;
        private System.Windows.Forms.Label lbCrmUser;
        private System.Windows.Forms.TextBox tbCrmPassword;
        private System.Windows.Forms.Label lbCrmPassword;
        private System.Windows.Forms.TextBox tbCrmUrl;
        private System.Windows.Forms.Label lbCrmUrl;
        private System.Windows.Forms.TextBox tbLogFile;
        private System.Windows.Forms.Label lbLogFile;
        private System.Windows.Forms.TextBox tbPrefixAttr;
        private System.Windows.Forms.Label lbPrefixAttr;
        private System.Windows.Forms.TextBox tbPrefixText;
        private System.Windows.Forms.Label lbPrefixText;
        private System.Windows.Forms.Button bPickLogFile;
        private System.Windows.Forms.Label lbCommand;
        private System.Windows.Forms.TextBox tbCommand;
        private System.Windows.Forms.OpenFileDialog ofdExcel;
        private System.Windows.Forms.OpenFileDialog ofdLog;
    }
}

