﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlfaPeople.Widex.DuplicateMerge.Ui
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void tbInput_TextChanged(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("DuplicateMerge.exe ");
            sb.Append($"-f \"{tbFileName.Text}\" ");
            sb.Append($"-u \"{tbCrmUser.Text}\" ");
            sb.Append($"-p \"{tbCrmPassword.Text}\" ");
            sb.Append($"-a \"{tbCrmUrl.Text}\" ");
            if(!String.IsNullOrWhiteSpace(tbLogFile.Text)) sb.Append($"-l \"{tbLogFile.Text}\" ");
            if (!String.IsNullOrWhiteSpace(tbPrefixAttr.Text)) sb.Append($"-r \"{tbPrefixAttr.Text}\" ");
            if (!String.IsNullOrWhiteSpace(tbPrefixText.Text)) sb.Append($"-t \"{tbPrefixText.Text}\"");

            tbCommand.Text = sb.ToString();
        }

        private void bPickFileName_Click(object sender, EventArgs e)
        {
            if (ofdExcel.ShowDialog() == DialogResult.OK)
            {
                tbFileName.Text = ofdExcel.FileName;
            }
        }

        private void bPickLogFile_Click(object sender, EventArgs e)
        {
            if (ofdLog.ShowDialog() == DialogResult.OK)
            {
                tbLogFile.Text = ofdLog.FileName;
            }
        }
    }
}
